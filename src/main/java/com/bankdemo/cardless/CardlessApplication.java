package com.bankdemo.cardless;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.StringVendorExtension;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collection;

@SpringBootApplication
@EnableSwagger2
public class CardlessApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CardlessApplication.class, args);
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select().apis(RequestHandlerSelectors.basePackage("com.bankdemo.cardless.controllers"))
				.paths(PathSelectors.any())
				.build()
				.enable(true)
				.apiInfo(apiInfo());
	}

	public ApiInfo apiInfo() {
		StringVendorExtension vendorExtension = new StringVendorExtension("", "");
		Collection<VendorExtension> vendorExtensions = new ArrayList<>();
		vendorExtensions.add(vendorExtension);

		Contact contactInfo = new Contact("La onda", "www.laonda.com",
				"clesel@gmail.com");

		return new ApiInfo(
				"Cardless API - POC",
				"Cardless API for AWS Training. Proof of Concept",
				"1.0",
				"For Demo only",
				contactInfo,
				"Apache 2.0",
				"www.apache.org",
				vendorExtensions);
	}

}
