package com.bankdemo.cardless.config;

import com.bankdemo.cardless.controllers.Accounts;
import com.bankdemo.cardless.controllers.Customers;
import com.bankdemo.cardless.controllers.Withdrawals;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.config.SwaggerConfigLocator;
import io.swagger.jaxrs.config.SwaggerContextService;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/api/v1")
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(Customers.class);
        register(Accounts.class);
        register(Withdrawals.class);

        configureSwagger();
    }

    private void configureSwagger() {
        BeanConfig swaggerConfig = new BeanConfig();
        swaggerConfig.setBasePath("/api/v1");
        SwaggerConfigLocator.getInstance().putConfig(SwaggerContextService.CONFIG_ID_DEFAULT, swaggerConfig);

        register(ApiListingResource.class);
        register(SwaggerSerializers.class);
    }
}
