package com.bankdemo.cardless.controllers;

import com.bankdemo.cardless.domain.Account;
import com.bankdemo.cardless.domain.Customer;
import com.bankdemo.cardless.dto.AccountCreationRequest;
import com.bankdemo.cardless.dto.AccountsResponse;
import com.bankdemo.cardless.dto.ErrorMessageResponse;
import com.bankdemo.cardless.repositories.AccountRepository;
import com.bankdemo.cardless.repositories.CustomerRepository;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Component
@Path("/accounts")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "accounts", produces = "application/json")
public class Accounts {

    private static final Logger log = LoggerFactory.getLogger(Accounts.class);

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Transactional
    @POST
    @Path("/customers/{customerId}")
    @ApiOperation(
            value = "create a new customer account",
            response = AccountsResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Resource found"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    public Response createAccount(@ApiParam @PathParam("customerId") long consumerId,
                                  @ApiParam AccountCreationRequest accountCreationRequest) {
        Optional<Customer> customerOptional = customerRepository.findById(consumerId);

        if (customerOptional.isPresent()) {
            Customer customer = customerOptional.get();

            Account account = new Account(
                    accountCreationRequest.getName(), accountCreationRequest.getProductType(),
                    accountCreationRequest.getEndingDigits(), accountCreationRequest.getBalance());
            account.setCustomer(customer);
            accountRepository.save(account);

            return Response.status(Response.Status.OK).entity(new AccountsResponse(customer)).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessageResponse("consumerId not found")).build();
        }
    }

    @Transactional
    @GET
    @Path("/customers/{customerId}")
    @ApiOperation(
            value = "Get customer accounts",
            response = AccountsResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Resource found"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    public Response getAccounts(@ApiParam @PathParam("customerId") long consumerId) {
        Optional<Customer> customerOptional = customerRepository.findById(consumerId);

        if (customerOptional.isPresent()) {
            Customer customer = customerOptional.get();

            return Response.status(Response.Status.OK).entity(new AccountsResponse(customer)).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessageResponse("consumerId not found")).build();
        }
    }
}
