package com.bankdemo.cardless.controllers;


import com.bankdemo.cardless.domain.Customer;
import com.bankdemo.cardless.dto.AccountsResponse;
import com.bankdemo.cardless.dto.CustomerInfoResponse;
import com.bankdemo.cardless.dto.CustomerRequest;
import com.bankdemo.cardless.dto.ErrorMessageResponse;
import com.bankdemo.cardless.repositories.CustomerRepository;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Component
@Path("/customers")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "customers", produces = "application/json")
public class Customers {

    private static final Logger log = LoggerFactory.getLogger(Customers.class);

    @Autowired
    private CustomerRepository customerRepository;

    @POST
    @ApiOperation(
            value = "Create new Customer",
            response = AccountsResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Resource found"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    public Response createCustomer(@ApiParam CustomerRequest customerRequest) {

        Customer customer = new Customer(customerRequest.getName());
        customerRepository.save(customer);

        return Response.status(Response.Status.OK).entity(new CustomerInfoResponse(customer)).build();
    }

    @GET
    @Path("/{customerId}")
    @ApiOperation(
            value = "Get customer info",
            response = AccountsResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Resource found"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    public Response getCustomerInfo(@ApiParam @PathParam("customerId") long consumerId) {
        Optional<Customer> customerOptional = customerRepository.findById(consumerId);

        if (customerOptional.isPresent()) {
            Customer customer = customerOptional.get();
            return Response.status(Response.Status.OK).entity(new CustomerInfoResponse(customer)).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessageResponse("consumerId not found")).build();
        }
    }
}
