package com.bankdemo.cardless.controllers;

import com.bankdemo.cardless.domain.Account;
import com.bankdemo.cardless.domain.WithdrawalCode;
import com.bankdemo.cardless.dto.AccountsResponse;
import com.bankdemo.cardless.dto.CodeRequest;
import com.bankdemo.cardless.dto.CodeResponse;
import com.bankdemo.cardless.dto.ErrorMessageResponse;
import com.bankdemo.cardless.repositories.AccountRepository;
import com.bankdemo.cardless.repositories.WithdrawalCodeRepository;
import io.swagger.annotations.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.Optional;

@Component
@Path("/withdrawals")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "withdrawals", produces = "application/json")
public class Withdrawals {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private WithdrawalCodeRepository withdrawalCodeRepository;


    @GET
    @Path("/{code}")
    @ApiOperation(
            value = "Get withdrawal code info",
            response = AccountsResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Resource found"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    public Response getWithdrawalCode(@ApiParam @PathParam("code") String code) {

        WithdrawalCode searchCode = new WithdrawalCode();
        searchCode.setCode(code);
        searchCode.setDisabled(false);

        ExampleMatcher searchCodeMatcher = ExampleMatcher.matching()
                .withIgnorePaths("amount");

        Optional<WithdrawalCode> withdrawalCodeOptional = withdrawalCodeRepository.findOne(
                Example.of(searchCode, searchCodeMatcher));

        if (withdrawalCodeOptional.isPresent()) {
            return Response.status(Response.Status.OK).entity(new CodeResponse(withdrawalCodeOptional.get())).build();

        } else {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessageResponse("withdrawal code not found")).build();
        }
    }

    @POST
    @Path("/accounts/{accountId}")
    @ApiOperation(
            value = "Generate a withdrawal code",
            response = CodeResponse.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Resource found"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    public Response createWithdrawalCode(
            @ApiParam @PathParam("accountId") long accountId,
            @ApiParam(value = "Code generation request object") CodeRequest codeRequest) {
        Optional<Account> accountOptional = accountRepository.findById(accountId);

        if (accountOptional.isPresent()) {
            String generatedCode = RandomStringUtils.randomNumeric(16);
            WithdrawalCode code = new WithdrawalCode(
                    generatedCode,
                    LocalDateTime.now().plusDays(1),
                    accountOptional.get(),
                    codeRequest.getAmount());
            withdrawalCodeRepository.save(code);

            return Response.status(Response.Status.OK).entity(new CodeResponse(code)).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessageResponse("accountId not found")).build();
        }
    }

    @Transactional
    @DELETE
    @Path("/{code}")
    @ApiOperation(
            value = "Cancel a withdrawal code",
            response = AccountsResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Resource found"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    public Response cancelWithdrawalCode(@ApiParam @PathParam("code") String code) {
        WithdrawalCode searchCode = new WithdrawalCode();
        searchCode.setCode(code);

        ExampleMatcher searchCodeMatcher = ExampleMatcher.matching()
                .withIgnorePaths("amount");

        Optional<WithdrawalCode> withdrawalCodeOptional = withdrawalCodeRepository.findOne(
                Example.of(searchCode, searchCodeMatcher));

        if (withdrawalCodeOptional.isPresent()) {
            WithdrawalCode withdrawalCode = withdrawalCodeOptional.get();
            withdrawalCode.setDisabled(true);

            withdrawalCodeRepository.save(withdrawalCode);
            return Response.status(Response.Status.OK).build();

        } else {
            return Response.status(Response.Status.NOT_FOUND).entity(
                    new ErrorMessageResponse("withdrawal code not found")).build();
        }
    }
}
