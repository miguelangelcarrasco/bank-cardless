package com.bankdemo.cardless.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String productType;
    private String endingDigits;
    private double balance;

    @ManyToOne
    private Customer customer;

    @OneToMany(mappedBy = "account")
    private List<WithdrawalCode> withdrawalCodes = new ArrayList<>();

    protected Account() {
    }

    public Account(String name, String productType, String endingDigits, double balance) {
        this.name = name;
        this.productType = productType;
        this.endingDigits = endingDigits;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getProductType() {
        return productType;
    }

    public String getEndingDigits() {
        return endingDigits;
    }

    public double getBalance() {
        return balance;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<WithdrawalCode> getWithdrawalCodes() {
        return withdrawalCodes;
    }

    public void setWithdrawalCodes(List<WithdrawalCode> withdrawalCodes) {
        this.withdrawalCodes = withdrawalCodes;
    }
}
