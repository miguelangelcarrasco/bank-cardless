package com.bankdemo.cardless.domain;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class WithdrawalCode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private LocalDateTime expirationDate;

    private int amount;

    boolean disabled = false;

    @ManyToOne
    private Account account;

    public WithdrawalCode() {
    }

    public WithdrawalCode(String code, LocalDateTime expirationDate, Account account, int amount) {
        this.code = code;
        this.expirationDate = expirationDate;
        this.account = account;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public int getAmount() {
        return amount;
    }

    public Account getAccount() {
        return account;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
