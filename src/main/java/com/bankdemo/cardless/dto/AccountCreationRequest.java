package com.bankdemo.cardless.dto;

public class AccountCreationRequest {
    private String name;
    private String productType;
    private String endingDigits;
    private double balance;

    public AccountCreationRequest(String name, String productType, String endingDigits, double balance) {
        this.name = name;
        this.productType = productType;
        this.endingDigits = endingDigits;
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public String getProductType() {
        return productType;
    }

    public String getEndingDigits() {
        return endingDigits;
    }

    public double getBalance() {
        return balance;
    }
}
