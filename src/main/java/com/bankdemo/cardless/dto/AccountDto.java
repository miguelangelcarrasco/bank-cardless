package com.bankdemo.cardless.dto;

import com.bankdemo.cardless.domain.Account;

public class AccountDto {
    private long accountId;
    private String name;
    private String productType;
    private String endingDigits;
    private double balance;

    public AccountDto(Account account) {
        this.accountId = account.getId();
        this.name = account.getName();
        this.productType = account.getProductType();
        this.endingDigits = account.getEndingDigits();
        this.balance = account.getBalance();
    }

    public long getAccountId() {
        return accountId;
    }

    public String getName() {
        return name;
    }

    public String getProductType() {
        return productType;
    }

    public String getEndingDigits() {
        return endingDigits;
    }

    public double getBalance() {
        return balance;
    }
}
