package com.bankdemo.cardless.dto;

import com.bankdemo.cardless.controllers.Accounts;
import com.bankdemo.cardless.domain.Account;
import com.bankdemo.cardless.domain.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class AccountsResponse {
    private long consumerId;
    private List<AccountDto> accounts=new ArrayList<>();


    private static final Logger log = LoggerFactory.getLogger(Accounts.class);

    public AccountsResponse(Customer customer, List<Account> accounts) {
        this.consumerId = customer.getId();
    }

    public AccountsResponse(Customer customer) {
        this.consumerId = customer.getId();
        List<Account> accounts = customer.getAccounts();
        for (Account account : accounts) {
            this.accounts.add(new AccountDto(account));
        }
    }

    public long getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(int consumerId) {
        this.consumerId = consumerId;
    }

    public List<AccountDto> getAccounts() {
        return accounts;
    }

}
