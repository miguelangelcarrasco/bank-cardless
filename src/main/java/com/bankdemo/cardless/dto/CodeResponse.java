package com.bankdemo.cardless.dto;

import com.bankdemo.cardless.domain.WithdrawalCode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDateTime;

public class CodeResponse {
    private String code;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime expirationDate;
    private int amount;

    public CodeResponse(String code, LocalDateTime expirationDate, int amount) {
        this.code = code;
        this.expirationDate = expirationDate;
        this.amount = amount;
    }

    public CodeResponse(WithdrawalCode withdrawalCode) {
        this.code = withdrawalCode.getCode();
        this.expirationDate = withdrawalCode.getExpirationDate();
        this.amount = withdrawalCode.getAmount();
    }

    public String getCode() {
        return code;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public double getAmount() {
        return amount;
    }
}
