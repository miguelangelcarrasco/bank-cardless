package com.bankdemo.cardless.dto;

import com.bankdemo.cardless.domain.Customer;

public class CustomerInfoResponse {
    private long costumerId;
    private String name;

    public CustomerInfoResponse(long consumerId, String name) {
        this.costumerId = consumerId;
        this.name = name;
    }

    public CustomerInfoResponse(Customer customer) {
        this.name = customer.getName();
        this.costumerId = customer.getId();
    }

    public long getCostumerId() {
        return costumerId;
    }

    public String getName() {
        return name;
    }
}
