package com.bankdemo.cardless.dto;

public class ErrorMessageResponse {
    //TODO ponerle un Enum con el tipo de error también.

    private final String message;

    public ErrorMessageResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
