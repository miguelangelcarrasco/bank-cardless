package com.bankdemo.cardless.repositories;

import com.bankdemo.cardless.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
