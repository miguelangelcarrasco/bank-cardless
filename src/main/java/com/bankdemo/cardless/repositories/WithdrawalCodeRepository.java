package com.bankdemo.cardless.repositories;

import com.bankdemo.cardless.domain.WithdrawalCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WithdrawalCodeRepository extends JpaRepository<WithdrawalCode, Long> {
}
