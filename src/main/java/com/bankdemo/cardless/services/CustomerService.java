package com.bankdemo.cardless.services;

import com.bankdemo.cardless.domain.Customer;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CustomerService {

    @PersistenceContext
    private EntityManager entityManager;

    public long insert(Customer customer) {
        entityManager.persist(customer);
        return customer.getId();
    }

    public Customer find(long id) {
        return entityManager.find(Customer.class, id);
    }

    public List<Customer> findAll() {
        Query query = entityManager.createNamedQuery(
                "query_find_all_customers", Customer.class);
        return query.getResultList();
    }
}
